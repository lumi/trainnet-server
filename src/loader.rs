use std::{fs::File, path::Path};

use serde::{Deserialize, Serialize};
use vek::Vec2;

use crate::{rail::RailGraph, shape::Shape, world::World};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct WorldNode {
    pub pos: (f64, f64),
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct WorldLink {
    pub start: u32,
    pub end: u32,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct WorldShape {
    #[serde(rename = "type")]
    pub type_: u8,
    pub points: Vec<(f64, f64)>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct WorldData {
    pub nodes: Vec<WorldNode>,
    pub links: Vec<WorldLink>,
    pub shapes: Vec<WorldShape>,
}

pub fn load_world(path: impl AsRef<Path>) -> anyhow::Result<World> {
    let mut file = File::open(path)?;

    let data: WorldData = serde_cbor::from_reader(&mut file)?;

    let mut rail = RailGraph::new();

    let mut nodes = Vec::new();

    for node in data.nodes {
        nodes.push(rail.create(Vec2::from(node.pos)));
    }

    for link in data.links {
        rail.link(nodes[link.start as usize], nodes[link.end as usize]);
    }

    rail.rebuild_blocks();

    let mut world = World::new(rail);

    for shape in data.shapes {
        world.shapes.create_shape(Shape {
            type_: shape.type_,
            points: shape
                .points
                .into_iter()
                .map(|p| Vec2::new(p.0, p.1))
                .collect(),
        });
    }

    Ok(world)
}
