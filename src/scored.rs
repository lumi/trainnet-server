use std::cmp::Ordering;

pub struct Scored<T>(pub f64, pub T);

impl<T> Eq for Scored<T> {}

impl<T> PartialEq for Scored<T> {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

impl<T> Ord for Scored<T> {
    fn cmp(&self, other: &Self) -> Ordering {
        other.0.total_cmp(&self.0)
    }
}

impl<T> PartialOrd for Scored<T> {
    fn partial_cmp(&self, other: &Scored<T>) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
