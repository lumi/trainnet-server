use std::time::Duration;

use trainnet_server::{
    conn::socket_handler,
    loader::load_world,
    rail::{NodeId, RailGraph, TrackLoc, Wagon},
    sim::Sim,
    world::World,
};

use rand::{
    distributions::uniform::{SampleRange, SampleUniform},
    Rng,
};
use tokio::net::TcpListener;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    pretty_env_logger::init();
    dotenv::dotenv().ok();

    let world_path = dotenv::var("WORLD_FILE")?;
    let listen_addr = dotenv::var("LISTEN_ADDR")?;
    let start_node = dotenv::var("START_NODE")
        .ok()
        .map(|t| t.parse())
        .transpose()?;
    let max_trains: usize = dotenv::var("TRAIN_COUNT")?.parse()?;

    let world = load_world(&world_path).unwrap_or_else(|_| World::new(RailGraph::new()));

    let sim = Sim::spawn(world);

    let mut train_count = 0;

    let rail = sim.world.lock().await.rail.clone();

    let spawner_sim = sim.clone();

    fn rand_range<T: SampleUniform>(range: impl SampleRange<T>) -> T {
        let mut rng = rand::thread_rng();
        rng.gen_range(range)
    }

    tokio::spawn(async move {
        while train_count < max_trains {
            let start = if let Some(start_node) = start_node {
                NodeId::from_u32(start_node)
            } else {
                NodeId::from_u32(rand_range(0..rail.nodes.len()) as u32)
            };

            let (end, _link) = rail.get_links(start).next().unwrap();

            let mut wagons = Vec::new();

            for _ in 0..rand_range(4..30) {
                wagons.push(Wagon::new(10.));
            }

            let loc = TrackLoc {
                start,
                end,
                along: 0.,
            };

            let speed = rand_range(3. ..30.);

            let _ = spawner_sim
                .commands_tx
                .send(trainnet_server::sim::SimCommand::CreateTrain { loc, speed, wagons })
                .await;

            train_count += 1;

            tokio::time::sleep(Duration::from_secs(3)).await;
        }
    });

    let lis = TcpListener::bind(&listen_addr).await?;

    loop {
        let Ok((conn, _addr)) = lis.accept().await else {
            break;
        };

        let sim = sim.clone();

        tokio::spawn(async move {
            if let Err(err) = socket_handler(sim, conn).await {
                log::error!("{}", err);
            }
        });
    }

    Ok(())
}
