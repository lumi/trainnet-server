use std::collections::{btree_map::Entry, BTreeMap, BinaryHeap};

use vek::Vec2;

use crate::scored::Scored;

use super::{
    blocks::{BlockId, Blocks},
    links::Links,
    node::{Link, NodeId, Nodes},
    track::TrackLoc,
    train::{Train, Wagon, WAGON_GAP},
};

#[derive(Debug, Clone)]
pub struct RailGraph {
    pub nodes: Nodes,
    pub links: Links,
    pub blocks: Blocks,
}

impl RailGraph {
    pub fn new() -> RailGraph {
        RailGraph {
            nodes: Nodes::new(),
            links: Links::new(),
            blocks: Blocks::new(),
        }
    }

    pub fn create_train(&self, id: usize, loc: TrackLoc, mut wagons: Vec<Wagon>) -> Train {
        let start = self.nodes[loc.start].pos;
        let end = self.nodes[loc.end].pos;
        let delta = end - start;
        let dir = delta.normalized();
        let pos = start + delta * loc.along;
        let length = wagons.iter().map(|w| w.length + WAGON_GAP).sum();
        for wagon in &mut wagons {
            wagon.pos = pos;
            wagon.dir = dir;
        }
        Train {
            id,
            loc,
            pos,
            dir,
            speed: 0.,
            target: None,
            path: None,
            length,
            trail: vec![start],
            wagons,
        }
    }

    pub fn create(&mut self, pos: Vec2<f64>) -> NodeId {
        self.nodes.create(pos)
    }

    pub fn get_link(&self, a: NodeId, b: NodeId) -> Option<&Link> {
        self.links.get_link(a, b)
    }

    pub fn get_links<'a>(&'a self, a: NodeId) -> impl Iterator<Item = (NodeId, &'a Link)> + 'a {
        self.links.get_links(a)
    }

    pub fn link(&mut self, a: NodeId, b: NodeId) {
        let apos = self.nodes[a].pos;
        let bpos = self.nodes[b].pos;
        let delta = bpos - apos;
        let dir = delta.normalized();
        let distance = delta.magnitude();
        self.links.add_link(a, b, Link { distance, dir })
    }

    pub fn rebuild_blocks(&mut self) {
        log::info!("rebuilding rail blocks");
        self.blocks = Blocks::build_from(&self);
        log::info!(
            "rebuilt {} nodes and {} links into {} blocks",
            self.nodes.len(),
            self.links.len(),
            self.blocks.len()
        );
    }

    pub fn find_node_path(
        &self,
        start: NodeId,
        start_dir: Vec2<f64>,
        end: NodeId,
    ) -> Option<Vec<NodeId>> {
        let mut heap = BinaryHeap::new();
        let mut prev = BTreeMap::new();

        struct State {
            distance: f64,
            node: NodeId,
            dir: Vec2<f64>,
        }

        heap.push(Scored(
            0.,
            State {
                distance: 0.,
                node: start,
                dir: start_dir,
            },
        ));

        while let Some(Scored(_, state)) = heap.pop() {
            if state.node == end {
                let mut out = Vec::new();

                let mut current = end;

                while current != start {
                    out.push(current);
                    current = prev[&current];
                }

                out.push(start);

                return Some(out);
            }
            let start_pos = self.nodes[state.node].pos;
            for (end, link) in self.get_links(state.node) {
                if state.dir.dot(link.dir) < 0.5 {
                    continue;
                }
                match prev.entry(end) {
                    Entry::Vacant(entry) => {
                        entry.insert(state.node);
                    }
                    Entry::Occupied(_entry) => continue,
                }
                let distance = state.distance + link.distance;
                let end_pos = self.nodes[state.node].pos;
                let heuristic = start_pos.distance(end_pos);
                heap.push(Scored(
                    distance + heuristic,
                    State {
                        distance,
                        node: end,
                        dir: link.dir,
                    },
                ));
            }
        }

        None
    }

    pub fn find_block_path(
        &self,
        start_loc: TrackLoc,
        end_loc: TrackLoc,
        get_cost: impl Fn(BlockId) -> Option<f64>,
    ) -> Option<Vec<BlockId>> {
        let mut heap = BinaryHeap::new();
        let mut prev: BTreeMap<BlockId, BlockId> = BTreeMap::new();

        log::debug!("finding path from {start_loc:?} to {end_loc:?}");

        let Some((start_dir, start_block_id)) = self.blocks.find_block(start_loc) else {
            log::debug!("could not find start block");
            return None;
        };

        log::debug!("found start block {start_dir:?} {start_block_id:?}");

        let Some((end_dir, end_block_id)) = self.blocks.find_block(end_loc) else {
            log::debug!("could not find end block");
            return None;
        };

        log::debug!("found end block {end_dir:?} {end_block_id:?}");

        let start_block = &self.blocks[start_block_id];

        let start_block_exit = start_block.exit_in_dir(start_dir);

        let start_pos = self.nodes.get_loc_position(start_loc);
        let end_pos = self.nodes.get_loc_position(end_loc);

        log::debug!("start pos {start_pos:?} end pos {end_pos:?}");

        struct State {
            distance: f64,
            block: BlockId,
            node: NodeId,
            speculative_since: Option<BlockId>,
            dir: Vec2<f64>,
        }

        heap.push(Scored(
            0.,
            State {
                distance: 0.,
                block: start_block.id,
                node: start_block_exit.node,
                speculative_since: None,
                dir: start_block_exit.dir,
            },
        ));

        while let Some(Scored(_, state)) = heap.pop() {
            for (block_dir, block_id) in self.blocks.find_blocks(state.node) {
                let mut speculative = state.speculative_since.is_some();
                let cost = if let Some(cost) = get_cost(block_id) {
                    cost
                } else {
                    speculative = true;
                    // TODO: what cost to use here?
                    0.
                };
                match prev.entry(block_id) {
                    Entry::Vacant(entry) => {
                        entry.insert(state.block);
                    }
                    Entry::Occupied(_entry) => continue,
                }
                if block_id == end_block_id {
                    let mut path = Vec::new();

                    let mut current = state.speculative_since.unwrap_or(end_block_id);

                    while current != start_block_id {
                        path.push(current);
                        current = prev[&current];
                    }

                    log::debug!("found path {path:?}");

                    return Some(path);
                }
                let block = &self.blocks[block_id];
                let block_exit = block.exit_in_dir(block_dir);
                if state.dir.dot(block_exit.dir) < 0.5 {
                    continue;
                }
                let distance = state.distance + block.distance + cost;
                let exit_pos = self.nodes[block_exit.node].pos;
                let heuristic = exit_pos.distance(end_pos);
                heap.push(Scored(
                    distance + heuristic,
                    State {
                        distance,
                        block: block_id,
                        node: block_exit.node,
                        speculative_since: state.speculative_since.or(if speculative {
                            Some(block_id)
                        } else {
                            None
                        }),
                        dir: block_exit.dir,
                    },
                ));
            }
        }

        log::debug!("no path found");

        None
    }
}
