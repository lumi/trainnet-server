use super::node::NodeId;

#[derive(Debug, Clone, Copy)]
pub struct TrackLoc {
    pub start: NodeId,
    pub end: NodeId,
    pub along: f64,
}

impl TrackLoc {
    pub fn new(start: NodeId, end: NodeId, along: f64) -> TrackLoc {
        TrackLoc { start, end, along }
    }
}
