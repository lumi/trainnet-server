use vek::Vec2;

use super::{blocks::BlockId, graph::RailGraph, reservations::Reservations, track::TrackLoc};

pub const WAGON_GAP: f64 = 1.;

#[derive(Debug, Clone)]
pub struct Wagon {
    pub length: f64,
    pub pos: Vec2<f64>,
    pub dir: Vec2<f64>,
}

impl Wagon {
    pub fn new(length: f64) -> Wagon {
        Wagon {
            length,
            pos: Vec2::new(0., 0.),
            dir: Vec2::new(0., 1.),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Train {
    pub id: usize,
    pub loc: TrackLoc,
    pub pos: Vec2<f64>,
    pub dir: Vec2<f64>,
    pub speed: f64,
    pub target: Option<TrackLoc>,
    pub path: Option<Vec<BlockId>>,
    pub length: f64,
    pub trail: Vec<Vec2<f64>>,
    pub wagons: Vec<Wagon>,
}

impl Train {
    fn truncate_trail(&mut self) {
        let mut total = 0.;
        let mut pos = self.pos;
        let mut retain = 0;
        for &tpos in self.trail.iter().rev() {
            retain += 1;
            let distance = pos.distance(tpos);
            total += distance;
            if total > self.length {
                break;
            }
            pos = tpos;
        }
        self.trail.drain(0..self.trail.len() - retain);
    }

    pub fn step(&mut self, graph: &RailGraph, reservations: &mut Reservations) {
        step_train(graph, reservations, self);
    }
}

pub fn step_train(graph: &RailGraph, reservations: &mut Reservations, train: &mut Train) {
    if let Some(target) = train.target {
        if train.path.is_none() {
            log::info!(
                "no path! computing path between {:?} and {:?}",
                train.loc,
                target
            );
            let get_cost = |block_id| {
                if let Some(train_id) = reservations.get(block_id) {
                    if train_id == train.id {
                        Some(0.)
                    } else {
                        None
                    }
                } else {
                    Some(0.)
                }
            };
            if let Some(mut path) = graph.find_block_path(train.loc, target, get_cost) {
                log::info!("found path: len={}", path.len());
                // TODO: reserve based on true stopping distance
                if !path.is_empty() {
                    let stopping_distance = 1000.;
                    let mut total = 0.;
                    let mut i = path.len() - 1;
                    loop {
                        let block_id = path[i];
                        let block = &graph.blocks[block_id];
                        total += block.distance;
                        if total > stopping_distance {
                            break;
                        }
                        if i == 0 {
                            break;
                        }
                        i -= 1;
                    }
                    path.drain(0..i);
                    for &block_id in &path {
                        reservations.reserve(block_id, train.id);
                    }
                }
                train.path = Some(path);
            } else {
                train.target = None;
            }
        }
    }

    train.loc.along += train.speed;

    loop {
        let a = &graph.nodes[train.loc.start];
        let b = &graph.nodes[train.loc.end];

        let link = graph.get_link(a.id, b.id).unwrap();

        let dist = link.distance;

        if train.loc.along < dist {
            break;
        }

        let mut preferred = None;

        if graph.blocks.is_knot(train.loc.end) {
            log::debug!("encountered knot");
            if let Some((_, current_block_id)) = graph.blocks.find_block(train.loc) {
                reservations.release(current_block_id);
            }
            if let Some(path) = &mut train.path {
                'outer: while let Some(&next) = path.last() {
                    for (block_dir, block_id) in graph.blocks.find_blocks(train.loc.end) {
                        if block_id != next {
                            continue;
                        }
                        let block = &graph.blocks[block_id];
                        let exit = block.exit_in_dir(block_dir.invert());
                        preferred = Some(exit.last);
                        break 'outer;
                    }
                    path.pop();
                    log::warn!("skipping block {next:?}");
                }
                if path.is_empty() {
                    train.path = None;
                }
            }
            log::debug!("preferred node {:?}", preferred);
        }

        if train.path.is_none() {
            train.loc.along = dist;
            break;
        }

        let mut best_out = None;
        let mut best_dot = -1.;

        for (end, _link) in graph.get_links(train.loc.end) {
            let c = &graph.nodes[end];

            let dot = (b.pos - a.pos)
                .normalized()
                .dot((c.pos - b.pos).normalized());

            if dot < 0.5 {
                continue;
            }

            if let Some(preferred_id) = preferred {
                if preferred_id == end {
                    best_out = Some(preferred_id);
                    log::debug!("got preferred");
                    break;
                }
            }

            if dot > best_dot {
                best_dot = dot;
                best_out = Some(end);
            }
        }

        if let Some(out) = best_out {
            train.trail.push(graph.nodes[train.loc.start].pos);
            train.truncate_trail();
            train.loc.start = train.loc.end;
            train.loc.end = out;
            train.loc.along -= dist;
        } else {
            train.loc.along = dist;
            break;
        }
    }

    if let Some((_, block_id)) = graph.blocks.find_block(train.loc) {
        reservations.reserve(block_id, train.id);
    }

    {
        let a = &graph.nodes[train.loc.start];
        let b = &graph.nodes[train.loc.end];
        let delta = b.pos - a.pos;
        train.dir = delta.normalized();
        train.pos = a.pos + delta.normalized() * train.loc.along;
        let mut dist = 0.;
        for wagon in &mut train.wagons {
            let allocated = wagon.length + WAGON_GAP;
            if let Some((p, d)) = point_along_rev(train.pos, &train.trail, dist + allocated * 0.5) {
                wagon.pos = p;
                wagon.dir = d;
                dist += allocated;
            } else {
                break;
            }
        }
    }
}

fn point_along_rev(
    head: Vec2<f64>,
    curve: &[Vec2<f64>],
    along: f64,
) -> Option<(Vec2<f64>, Vec2<f64>)> {
    if curve.is_empty() {
        return None;
    }

    let mut left = along;
    let mut prev = head;

    for &next in curve.iter().rev() {
        let delta = prev - next;
        let dist = delta.magnitude();
        let dir = delta.normalized();
        left -= dist;
        if left < 0. {
            return Some((prev + (left + dist) * -dir, dir));
        }
        prev = next;
    }

    None
}
