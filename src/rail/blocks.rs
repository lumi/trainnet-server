use core::ops::{Index, IndexMut};
use std::collections::{BTreeMap, BTreeSet};

use vek::Vec2;

use super::{graph::RailGraph, node::NodeId, TrackLoc};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct BlockId(usize);

impl BlockId {
    pub fn as_u32(self) -> u32 {
        self.0 as u32
    }

    pub fn from_u32(num: u32) -> BlockId {
        BlockId(num as usize)
    }
}

#[derive(Debug, Clone)]
pub struct BlockExit {
    pub node: NodeId,
    pub last: NodeId,
    pub dir: Vec2<f64>,
}

#[derive(Debug, Clone)]
pub struct Block {
    pub id: BlockId,
    pub distance: f64,
    pub exits: [BlockExit; 2],
}

impl Block {
    pub fn exit_at(&self, node: NodeId) -> &BlockExit {
        if node == self.exits[0].node {
            &self.exits[0]
        } else if node == self.exits[1].node {
            &self.exits[1]
        } else {
            panic!("block has no exit at {node:?}");
        }
    }

    pub fn exit_in_dir(&self, dir: BlockDir) -> &BlockExit {
        match dir {
            BlockDir::Forward => &self.exits[1],
            BlockDir::Backward => &self.exits[0],
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum BlockDir {
    Forward,
    Backward,
}

impl BlockDir {
    pub fn invert(self) -> BlockDir {
        match self {
            BlockDir::Forward => BlockDir::Backward,
            BlockDir::Backward => BlockDir::Forward,
        }
    }
}

#[derive(Debug, Clone)]
pub struct FoundBlock {
    pub id: BlockId,
    pub start: NodeId,
    pub initial: NodeId,
    pub last: NodeId,
    pub end: NodeId,
}

#[derive(Debug, Clone)]
pub struct Blocks {
    pub blocks: Vec<Block>,
    pub knots: BTreeSet<NodeId>,
    pub block_links: BTreeMap<(NodeId, NodeId), Vec<(BlockDir, BlockId)>>,
    pub link_to_block: BTreeMap<(NodeId, NodeId), (BlockDir, BlockId)>,
}

impl Index<BlockId> for Blocks {
    type Output = Block;

    fn index(&self, BlockId(id): BlockId) -> &Block {
        &self.blocks[id]
    }
}

impl IndexMut<BlockId> for Blocks {
    fn index_mut(&mut self, BlockId(id): BlockId) -> &mut Block {
        &mut self.blocks[id]
    }
}

impl Blocks {
    pub fn new() -> Blocks {
        Blocks {
            blocks: Vec::new(),
            knots: BTreeSet::new(),
            block_links: BTreeMap::new(),
            link_to_block: BTreeMap::new(),
        }
    }

    pub fn len(&self) -> usize {
        self.blocks.len()
    }

    pub fn find_block(&self, loc: TrackLoc) -> Option<(BlockDir, BlockId)> {
        if let Some(&(dir, id)) = self.link_to_block.get(&(loc.start, loc.end)) {
            Some((dir, id))
        } else {
            None
        }
    }

    pub fn find_blocks<'a>(
        &'a self,
        node: NodeId,
    ) -> impl Iterator<Item = (BlockDir, BlockId)> + 'a {
        self.block_links
            .range((node, NodeId::MIN)..=(node, NodeId::MAX))
            .flat_map(|(_, keys)| keys.iter())
            .map(|&(dir, id)| (dir, id))
    }

    pub fn is_knot(&self, node: NodeId) -> bool {
        self.knots.contains(&node)
    }

    pub fn build_from(graph: &RailGraph) -> Blocks {
        let mut blocks = Vec::new();
        let mut knots = BTreeSet::new();
        let mut block_links = BTreeMap::new();
        let mut link_to_block = BTreeMap::new();

        let mut blocked = BTreeSet::new();
        let mut next_nodes = Vec::new();

        for node in &graph.nodes {
            let links: Vec<_> = graph.links.get_links(node.id).collect();

            if links.len() > 2 {
                knots.insert(node.id);
                for (initial, link) in links {
                    if blocked.contains(&(node.id, initial)) {
                        continue;
                    }
                    let block_id = BlockId(blocks.len());
                    let mut dist = link.distance;
                    let mut last = node.id;
                    let mut current = initial;
                    loop {
                        link_to_block.insert((last, current), (BlockDir::Forward, block_id));
                        link_to_block.insert((current, last), (BlockDir::Backward, block_id));

                        next_nodes.clear();

                        let next_links = graph.links.get_links(current).filter(|&(n, _)| n != last);

                        next_nodes.extend(next_links);

                        match next_nodes.len() {
                            0 => {
                                break;
                            }
                            1 => {
                                let (next, _) = next_nodes[0];
                                dist += link.distance;
                                last = current;
                                current = next;
                            }
                            _ => {
                                blocked.insert((last, current));
                                break;
                            }
                        }
                    }
                    blocked.insert((node.id, initial));
                    blocked.insert((current, last));
                    blocks.push(Block {
                        id: block_id,
                        distance: dist,
                        exits: [
                            BlockExit {
                                node: node.id,
                                last: initial,
                                dir: graph.links.get_link(initial, node.id).unwrap().dir,
                            },
                            BlockExit {
                                node: current,
                                last,
                                dir: graph.links.get_link(last, current).unwrap().dir,
                            },
                        ],
                    });
                    block_links
                        .entry((node.id, current))
                        .or_insert(Vec::new())
                        .push((BlockDir::Forward, block_id));
                    block_links
                        .entry((current, node.id))
                        .or_insert(Vec::new())
                        .push((BlockDir::Backward, block_id));
                }
            }
        }

        Blocks {
            blocks,
            knots,
            block_links,
            link_to_block,
        }
    }
}
