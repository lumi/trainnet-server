use std::collections::BTreeMap;

use super::blocks::BlockId;

#[derive(Debug, Clone)]
pub struct Reservations {
    blocks: BTreeMap<BlockId, usize>,
}

impl Reservations {
    pub fn new() -> Reservations {
        Reservations {
            blocks: BTreeMap::new(),
        }
    }

    pub fn reserve(&mut self, id: BlockId, train_id: usize) {
        self.blocks.insert(id, train_id);
    }

    pub fn release(&mut self, id: BlockId) {
        self.blocks.remove(&id);
    }

    pub fn get(&self, id: BlockId) -> Option<usize> {
        self.blocks.get(&id).map(|train_id| *train_id)
    }
}
