use std::collections::BTreeMap;

use super::node::{Link, NodeId};

#[derive(Debug, Clone)]
pub struct Links {
    pub links: BTreeMap<(NodeId, NodeId), Link>,
}

impl Links {
    pub fn new() -> Links {
        Links {
            links: BTreeMap::new(),
        }
    }

    pub fn len(&self) -> usize {
        self.links.len()
    }

    pub fn get_link(&self, a: NodeId, b: NodeId) -> Option<&Link> {
        self.links.get(&(a, b))
    }

    pub fn get_links<'a>(&'a self, a: NodeId) -> impl Iterator<Item = (NodeId, &'a Link)> + 'a {
        self.links
            .range((a, NodeId::MIN)..=(a, NodeId::MAX))
            .map(|(&(_, end), link)| (end, link))
    }

    pub fn add_link(&mut self, a: NodeId, b: NodeId, link: Link) {
        let link_rev = link.reverse();
        self.links.insert((a, b), link);
        self.links.insert((b, a), link_rev);
    }

    pub fn iter<'a>(&'a self) -> impl Iterator<Item = ((NodeId, NodeId), &'a Link)> + 'a {
        self.links.iter().map(|(&(a, b), l)| ((a, b), l))
    }
}
