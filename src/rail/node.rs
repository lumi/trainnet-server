use std::ops::{Index, IndexMut};

use vek::Vec2;

use super::TrackLoc;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct NodeId(usize);

impl NodeId {
    pub const MIN: NodeId = NodeId(0);
    pub const MAX: NodeId = NodeId(usize::MAX);

    pub fn as_u32(self) -> u32 {
        self.0 as u32
    }

    pub fn from_u32(num: u32) -> NodeId {
        NodeId(num as usize)
    }
}

#[derive(Debug, Clone)]
pub struct Link {
    pub distance: f64,
    pub dir: Vec2<f64>,
}

impl Link {
    pub fn new(distance: f64, dir: Vec2<f64>) -> Link {
        Link { distance, dir }
    }

    pub fn reverse(&self) -> Link {
        Link {
            distance: self.distance,
            dir: -self.dir,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Node {
    pub id: NodeId,
    pub pos: Vec2<f64>,
}

#[derive(Debug, Clone)]
pub struct Nodes {
    nodes: Vec<Node>,
}

impl Index<NodeId> for Nodes {
    type Output = Node;

    fn index(&self, NodeId(idx): NodeId) -> &Node {
        &self.nodes[idx]
    }
}

impl IndexMut<NodeId> for Nodes {
    fn index_mut(&mut self, NodeId(idx): NodeId) -> &mut Node {
        &mut self.nodes[idx]
    }
}

impl<'a> IntoIterator for &'a Nodes {
    type Item = &'a Node;
    type IntoIter = core::slice::Iter<'a, Node>;

    fn into_iter(self) -> core::slice::Iter<'a, Node> {
        self.nodes.iter()
    }
}

impl Nodes {
    pub fn new() -> Nodes {
        Nodes { nodes: Vec::new() }
    }

    pub fn create(&mut self, pos: Vec2<f64>) -> NodeId {
        let id = NodeId(self.nodes.len());
        self.nodes.push(Node { id, pos });
        id
    }

    pub fn len(&self) -> usize {
        self.nodes.len()
    }

    pub fn get_loc_position(&self, loc: TrackLoc) -> Vec2<f64> {
        let start_pos = self[loc.start].pos;
        let end_pos = self[loc.end].pos;
        let delta = end_pos - start_pos;
        let dir = delta.normalized();
        start_pos + dir * loc.along
    }
}
