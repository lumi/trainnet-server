use std::collections::BTreeSet;

use futures::{SinkExt, StreamExt};
use tokio::net::TcpStream;
use tokio_tungstenite::{tungstenite::Message, WebSocketStream};
use vek::Vec2;

use crate::{
    protocol::{self, ClientFrame, ServerFrame, Writer},
    rail::NodeId,
    shape::{Aabb, ShapeId},
    sim::SimHandle,
    viewport::Viewport,
    world::World,
};

pub struct Conn {
    ws: WebSocketStream<TcpStream>,
}

impl Conn {
    pub async fn accept(stream: TcpStream) -> anyhow::Result<Conn> {
        let ws = tokio_tungstenite::accept_async(stream).await?;
        Ok(Conn { ws })
    }

    pub async fn send(&mut self, frame: &ServerFrame) -> anyhow::Result<()> {
        let mut writer = Writer::new();
        frame.dump(&mut writer);
        self.ws.send(Message::Binary(writer.into_vec())).await?;
        Ok(())
    }

    pub async fn recv(&mut self) -> anyhow::Result<ClientFrame> {
        while let Some(msg) = self.ws.next().await {
            match msg? {
                Message::Text(text) => {
                    let frame: ClientFrame = serde_json::from_str(&text)?;
                    return Ok(frame);
                }
                _ => {}
            }
        }

        Err(anyhow::anyhow!("eof"))
    }
}

struct Client {
    sent_nodes: BTreeSet<NodeId>,
    sent_edges: BTreeSet<(NodeId, NodeId)>,
    sent_shapes: BTreeSet<ShapeId>,
    viewport: Viewport,
}

fn create_dynamic_frame(world: &World, viewport: Viewport) -> ServerFrame {
    ServerFrame::Dynamic {
        time: world.time,
        trains: world
            .trains
            .iter()
            .filter(|t| viewport.contains(t.pos))
            .map(|t| protocol::Train {
                id: t.id as u32,
                pos: t.pos.into_tuple(),
                dir: t.dir.into_tuple(),
                speed: t.speed,
                path_length: t.path.as_ref().map(|p| p.len() as u16).unwrap_or(0),
                wagons: t
                    .wagons
                    .iter()
                    .map(|w| protocol::Wagon {
                        length: w.length,
                        pos: w.pos.into_tuple(),
                        dir: w.dir.into_tuple(),
                    })
                    .collect(),
            })
            .collect::<Vec<_>>(),
    }
}

pub async fn socket_handler(mut sim: SimHandle, stream: TcpStream) -> anyhow::Result<()> {
    let mut conn = Conn::accept(stream).await?;

    let mut client = Client {
        sent_nodes: BTreeSet::new(),
        sent_edges: BTreeSet::new(),
        sent_shapes: BTreeSet::new(),
        viewport: Viewport::empty(),
    };

    loop {
        tokio::select! {
            changes_result = sim.changes.changed() => {
                let _changes = changes_result?;
                let frame = {
                    let world = sim.world.lock().await;
                    create_dynamic_frame(&world, client.viewport)
                };
                conn.send(&frame).await?;
            }
            res = conn.recv() => match res {
                Ok(frame) => {
                    client.handle_msg(&mut sim, &mut conn, frame).await?;
                },
                Err(err) => {
                    log::error!("err {err}");
                    break Ok(());
                },
            }
        };
    }
}

impl Client {
    async fn handle_msg(
        &mut self,
        sim: &mut SimHandle,
        conn: &mut Conn,
        frame: ClientFrame,
    ) -> anyhow::Result<()> {
        match frame {
            ClientFrame::SetViewport {
                min_x,
                min_y,
                max_x,
                max_y,
            } => {
                self.viewport = Viewport {
                    min_x,
                    max_x,
                    min_y,
                    max_y,
                };

                let (rail, shapes) = {
                    let world = sim.world.lock().await;
                    (world.rail.clone(), world.shapes.clone())
                };

                let mut del_nodes = Vec::new();
                let mut del_links = Vec::new();
                let mut del_shapes = Vec::new();
                let mut add_nodes = Vec::new();
                let mut add_links = Vec::new();
                let mut add_shapes = Vec::new();

                for &node_id in &self.sent_nodes {
                    let node = &rail.nodes[node_id];

                    if !self.viewport.contains(node.pos) {
                        del_nodes.push(node_id.as_u32());
                    }
                }

                for &node_id in &del_nodes {
                    self.sent_nodes.remove(&NodeId::from_u32(node_id));
                }

                for &node_id in &del_nodes {
                    let nid = NodeId::from_u32(node_id);

                    for (end, _link) in rail.get_links(nid) {
                        if !self.sent_nodes.contains(&end) {
                            del_links.push((nid.as_u32(), end.as_u32()));
                            self.sent_edges.remove(&(nid, end));
                        }
                    }
                }

                for node in &rail.nodes {
                    if !self.viewport.contains(node.pos) {
                        continue;
                    }

                    if self.sent_nodes.insert(node.id) {
                        add_nodes.push((node.id.as_u32(), node.pos.into_tuple()));
                    }
                }

                for ((start, end), _link) in rail.links.iter() {
                    if !self.sent_nodes.contains(&start) || !self.sent_nodes.contains(&end) {
                        continue;
                    }

                    if self.sent_edges.insert((start, end)) {
                        let block_id = rail.blocks.link_to_block.get(&(start, end));
                        add_links.push((
                            (start.as_u32(), end.as_u32()),
                            block_id.map(|(_, b)| b.as_u32()).unwrap_or(0),
                        ));
                    }
                }

                let vp_aabb = Aabb::new(
                    Vec2::new(min_x, min_y),
                    Vec2::new(max_x - min_x, max_y - min_y),
                );

                for &id in &self.sent_shapes {
                    let aabb = shapes.get_aabb(id);

                    if !vp_aabb.intersects(aabb) {
                        del_shapes.push(id as u32);
                    }
                }

                for &id in &del_shapes {
                    self.sent_shapes.remove(&(id as usize));
                }

                for id in shapes.query_vec(vp_aabb) {
                    let shape = shapes.get(id);

                    if self.sent_shapes.insert(id) {
                        add_shapes.push((
                            id as u32,
                            shape.type_,
                            shape.points.iter().map(|p| (p.x, p.y)).collect(),
                        ));
                    }
                }

                if !add_nodes.is_empty()
                    || !add_links.is_empty()
                    || !add_shapes.is_empty()
                    || !del_nodes.is_empty()
                    || !del_links.is_empty()
                    || !del_shapes.is_empty()
                {
                    conn.send(&ServerFrame::Bulk {
                        del_nodes,
                        del_links,
                        del_shapes,
                        add_nodes,
                        add_links,
                        add_shapes,
                    })
                    .await?;
                }
            }
        }
        Ok(())
    }
}
