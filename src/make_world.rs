use std::{
    collections::HashMap,
    env,
    fs::File,
    io::{BufRead, BufReader},
};

use trainnet_server::loader::{WorldData, WorldLink, WorldNode, WorldShape};

fn main() -> anyhow::Result<()> {
    let mut args = env::args();

    let _exec = args.next();

    let file = File::open(args.next().unwrap())?;

    let reader = BufReader::new(file);

    let mut shapes = Vec::new();
    let mut nodes = Vec::new();
    let mut links = Vec::new();

    let mut node_map = HashMap::new();

    let mut last = None;

    for line in reader.lines() {
        let line = line?;

        if line.is_empty() {
            continue;
        }

        if line.starts_with("#") {
            continue;
        }

        let mut split = line.split(' ');

        let cmd = split.next().unwrap();

        match cmd {
            "shape" => {
                let type_ = split.next().unwrap().parse().unwrap();
                let cmd_args: Vec<f64> = split.map(|a| a.parse().unwrap()).collect();
                let mut points = Vec::new();
                for chunk in cmd_args.chunks(2) {
                    points.push((chunk[0], chunk[1]));
                }
                shapes.push(WorldShape { type_, points });
            }
            "node" => {
                let x: f64 = split.next().unwrap().parse().unwrap();
                let y: f64 = split.next().unwrap().parse().unwrap();
                let node_id = nodes.len() as u32;
                nodes.push(WorldNode { pos: (x, y) });
                if let Some(name) = split.next() {
                    node_map.insert(name.to_owned(), node_id);
                }
                last = Some(node_id);
            }
            "next" => {
                let x: f64 = split.next().unwrap().parse().unwrap();
                let y: f64 = split.next().unwrap().parse().unwrap();
                let node_id = nodes.len() as u32;
                nodes.push(WorldNode { pos: (x, y) });
                if let Some(name) = split.next() {
                    node_map.insert(name.to_owned(), node_id);
                }
                if let Some(last_id) = last {
                    links.push(WorldLink {
                        start: last_id,
                        end: node_id,
                    });
                } else {
                    panic!("no last node");
                }
                last = Some(node_id);
            }
            "move" => {
                let start = split.next().unwrap().to_owned();
                let start_id = node_map[&start];
                last = Some(start_id);
            }
            "link" => {
                let end = split.next().unwrap().to_owned();
                let end_id = node_map[&end];
                if let Some(last_id) = last {
                    links.push(WorldLink {
                        start: last_id,
                        end: end_id,
                    });
                } else {
                    panic!("no last node");
                }
            }
            _ => {
                panic!("invalid command {cmd}");
            }
        }
    }

    let data = WorldData {
        shapes,
        nodes,
        links,
    };

    let mut out = File::create(args.next().unwrap())?;

    serde_cbor::to_writer(&mut out, &data)?;

    Ok(())
}
