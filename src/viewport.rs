use vek::Vec2;

#[derive(Debug, Clone, Copy)]
pub struct Viewport {
    pub min_x: f64,
    pub max_x: f64,
    pub min_y: f64,
    pub max_y: f64,
}

impl Viewport {
    pub fn empty() -> Viewport {
        Viewport {
            min_x: 0.,
            max_x: 0.,
            min_y: 0.,
            max_y: 0.,
        }
    }

    pub fn contains(self, pos: Vec2<f64>) -> bool {
        let Viewport {
            min_x,
            max_x,
            min_y,
            max_y,
        } = self;
        pos.x >= min_x && pos.x <= max_x && pos.y >= min_y && pos.y <= max_y
    }
}
