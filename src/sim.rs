use std::{
    sync::Arc,
    thread,
    time::{Duration, Instant},
};

use tokio::sync::{mpsc, watch, Mutex};

use crate::{
    rail::{TrackLoc, Wagon},
    world::World,
};

#[derive(Debug, Clone)]
pub enum SimCommand {
    CreateTrain {
        loc: TrackLoc,
        speed: f64,
        wagons: Vec<Wagon>,
    },
}

#[derive(Clone)]
pub struct Changes {}

#[derive(Clone)]
pub struct SimHandle {
    pub world: Arc<Mutex<World>>,
    pub changes: watch::Receiver<Changes>,
    pub commands_tx: mpsc::Sender<SimCommand>,
}

pub struct Sim {
    world: Arc<Mutex<World>>,
    changes: watch::Sender<Changes>,
    commands_rx: mpsc::Receiver<SimCommand>,
}

impl Sim {
    pub fn spawn(world: World) -> SimHandle {
        let world = Arc::new(Mutex::new(world));
        let (changes_tx, changes_rx) = watch::channel(Changes {});
        let (commands_tx, commands_rx) = mpsc::channel(32);
        let me = Sim {
            world: world.clone(),
            changes: changes_tx,
            commands_rx,
        };
        thread::spawn(move || {
            me.run();
        });
        SimHandle {
            world,
            changes: changes_rx,
            commands_tx,
        }
    }

    fn run(mut self) {
        loop {
            let start = Instant::now();
            {
                let mut world = self.world.blocking_lock();
                while let Ok(cmd) = self.commands_rx.try_recv() {
                    match cmd {
                        SimCommand::CreateTrain { loc, speed, wagons } => {
                            let new_id = world.trains.len();
                            let mut train = world.rail.create_train(new_id, loc, wagons);
                            train.speed = speed;
                            world.trains.push(train);
                        }
                    }
                }
                world.step();
            }
            let _ = self.changes.send(Changes {});
            let elapsed = start.elapsed();
            let dur = Duration::from_millis(100);
            if elapsed >= dur {
                log::warn!("simulation overloaded, took {elapsed:?}");
                continue;
            }
            thread::sleep(dur - elapsed);
        }
    }
}
