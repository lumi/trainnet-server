use serde::{Deserialize, Serialize};

#[derive(Debug, Clone)]
pub struct Writer {
    out: Vec<u8>,
}

impl Writer {
    pub fn new() -> Writer {
        Writer { out: Vec::new() }
    }

    pub fn into_vec(self) -> Vec<u8> {
        self.out
    }

    pub fn write_u32(&mut self, num: u32) {
        self.out.extend_from_slice(&num.to_le_bytes()[..]);
    }

    pub fn write_u16(&mut self, num: u16) {
        self.out.extend_from_slice(&num.to_le_bytes()[..]);
    }

    pub fn write_u8(&mut self, num: u8) {
        self.out.extend_from_slice(&num.to_le_bytes()[..]);
    }

    pub fn write_i32(&mut self, num: i32) {
        self.out.extend_from_slice(&num.to_le_bytes()[..]);
    }

    pub fn write_i16(&mut self, num: i16) {
        self.out.extend_from_slice(&num.to_le_bytes()[..]);
    }

    pub fn write_i8(&mut self, num: i8) {
        self.out.extend_from_slice(&num.to_le_bytes()[..]);
    }

    pub fn write_f32(&mut self, num: f32) {
        self.write_i32((num * 1000.) as i32);
    }
}

#[derive(Debug, Clone)]
pub struct Wagon {
    pub length: f64,
    pub pos: (f64, f64),
    pub dir: (f64, f64),
}

#[derive(Debug, Clone)]
pub struct Train {
    pub id: u32,
    pub pos: (f64, f64),
    pub dir: (f64, f64),
    pub speed: f64,
    pub path_length: u16,
    pub wagons: Vec<Wagon>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(tag = "type", rename_all = "snake_case")]
pub enum ClientFrame {
    SetViewport {
        min_x: f64,
        max_x: f64,
        min_y: f64,
        max_y: f64,
    },
}

#[derive(Debug, Clone)]
pub enum ServerFrame {
    Dynamic {
        time: f64,
        trains: Vec<Train>,
    },
    Bulk {
        del_nodes: Vec<u32>,
        del_links: Vec<(u32, u32)>,
        del_shapes: Vec<u32>,
        add_nodes: Vec<(u32, (f64, f64))>,
        add_links: Vec<((u32, u32), u32)>,
        add_shapes: Vec<(u32, u8, Vec<(f64, f64)>)>,
    },
}

impl ServerFrame {
    pub fn dump(&self, out: &mut Writer) {
        match *self {
            ServerFrame::Dynamic { time, ref trains } => {
                out.write_u8(1);
                out.write_f32(time as f32);
                out.write_u16(trains.len() as u16);
                for train in trains {
                    out.write_u32(train.id);
                    out.write_f32(train.pos.0 as f32);
                    out.write_f32(train.pos.1 as f32);
                    out.write_f32(f64::atan2(train.dir.1, train.dir.0) as f32);
                    out.write_f32(train.speed as f32);
                    out.write_u16(train.path_length);
                    out.write_u8(train.wagons.len() as u8);
                    for wagon in &train.wagons {
                        out.write_f32(wagon.pos.0 as f32);
                        out.write_f32(wagon.pos.1 as f32);
                        out.write_f32(f64::atan2(wagon.dir.1, wagon.dir.0) as f32);
                        out.write_f32(wagon.length as f32);
                    }
                }
            }
            ServerFrame::Bulk {
                ref del_nodes,
                ref del_links,
                ref del_shapes,
                ref add_nodes,
                ref add_links,
                ref add_shapes,
            } => {
                out.write_u8(2);
                out.write_u32(del_nodes.len() as u32);
                out.write_u32(del_links.len() as u32);
                out.write_u32(del_shapes.len() as u32);
                out.write_u32(add_nodes.len() as u32);
                out.write_u32(add_links.len() as u32);
                out.write_u32(add_shapes.len() as u32);
                for &id in del_nodes {
                    out.write_u32(id);
                }
                for &(start, end) in del_links {
                    out.write_u32(start);
                    out.write_u32(end);
                }
                for &id in del_shapes {
                    out.write_u32(id);
                }
                for &(id, (x, y)) in add_nodes {
                    out.write_u32(id);
                    out.write_f32(x as f32);
                    out.write_f32(y as f32);
                }
                for &((start, end), block_id) in add_links {
                    out.write_u32(start);
                    out.write_u32(end);
                    out.write_u32(block_id);
                }
                for &(id, type_, ref points) in add_shapes {
                    out.write_u32(id);
                    out.write_u8(type_);
                    out.write_u32(points.len() as u32);
                    for &(x, y) in points {
                        out.write_f32(x as f32);
                        out.write_f32(y as f32);
                    }
                }
            }
        }
    }
}
