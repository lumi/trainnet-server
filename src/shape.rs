use vek::Vec2;

fn range_intersect(ami: f64, ama: f64, bmi: f64, bma: f64) -> bool {
    ami.max(bmi) <= ama.min(bma)
}

pub type ShapeId = usize;

#[derive(Debug, Clone, Copy)]
pub struct Aabb {
    pub start: Vec2<f64>,
    pub size: Vec2<f64>,
}

impl Aabb {
    pub fn new(start: Vec2<f64>, size: Vec2<f64>) -> Aabb {
        Aabb { start, size }
    }

    pub fn from_points(points: &[Vec2<f64>]) -> Aabb {
        let mut min_x = f64::INFINITY;
        let mut min_y = f64::INFINITY;
        let mut max_x = f64::NEG_INFINITY;
        let mut max_y = f64::NEG_INFINITY;

        for point in points {
            if point.x < min_x {
                min_x = point.x;
            }
            if point.y < min_y {
                min_y = point.y;
            }
            if point.x > max_x {
                max_x = point.x;
            }
            if point.y > max_y {
                max_y = point.y;
            }
        }

        let start = Vec2::new(min_x, min_y);
        let size = Vec2::new(max_x, max_y) - start;

        Aabb { start, size }
    }

    pub fn intersects(self, other: Aabb) -> bool {
        range_intersect(
            self.start.x,
            self.start.x + self.size.x,
            other.start.x,
            other.start.x + other.size.x,
        ) && range_intersect(
            self.start.y,
            self.start.y + self.size.y,
            other.start.y,
            other.start.y + other.size.y,
        )
    }
}

#[derive(Debug, Clone)]
pub struct Shape {
    pub type_: u8,
    pub points: Vec<Vec2<f64>>,
}

impl Shape {
    pub fn new(type_: u8, points: Vec<Vec2<f64>>) -> Shape {
        Shape { type_, points }
    }
}

#[derive(Debug, Clone)]
pub struct Shapes {
    shapes: Vec<Shape>,
    aabbs: Vec<Aabb>,
}

impl Shapes {
    pub fn new() -> Shapes {
        Shapes {
            shapes: Vec::new(),
            aabbs: Vec::new(),
        }
    }

    pub fn create_shape(&mut self, shape: Shape) -> ShapeId {
        let id = self.shapes.len();

        let aabb = Aabb::from_points(&shape.points);

        self.shapes.push(shape);
        self.aabbs.push(aabb);

        id
    }

    pub fn get(&self, shape_id: ShapeId) -> &Shape {
        &self.shapes[shape_id]
    }

    pub fn get_aabb(&self, shape_id: ShapeId) -> Aabb {
        self.aabbs[shape_id]
    }

    pub fn query<'a>(&self, mut output: impl FnMut(ShapeId), aabb: Aabb) {
        for shape_id in 0..self.shapes.len() {
            let shape_aabb = &self.aabbs[shape_id];
            if shape_aabb.intersects(aabb) {
                output(shape_id);
            }
        }
    }

    pub fn query_vec<'a>(&self, aabb: Aabb) -> Vec<ShapeId> {
        let mut shapes = Vec::new();

        self.query(|shape| shapes.push(shape), aabb);

        shapes
    }

    pub fn all(&self) -> &[Shape] {
        &self.shapes
    }
}
