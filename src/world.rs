use std::sync::Arc;

use rand::{seq::SliceRandom, Rng};

use crate::{
    rail::{NodeId, RailGraph, Reservations, TrackLoc, Train},
    shape::Shapes,
};

#[derive(Debug, Clone)]
pub struct World {
    pub time: f64,
    pub shapes: Shapes,
    pub rail: Arc<RailGraph>,
    pub trains: Vec<Train>,
    pub reservations: Reservations,
}

impl World {
    pub fn new(rail: RailGraph) -> World {
        World {
            time: 0.,
            shapes: Shapes::new(),
            rail: Arc::new(rail),
            trains: Vec::new(),
            reservations: Reservations::new(),
        }
    }

    pub fn step(&mut self) {
        let mut rng = rand::thread_rng();

        self.time += 0.1;

        for train in &mut self.trains {
            train.step(&self.rail, &mut self.reservations);
        }

        for train in &mut self.trains {
            if train.target.is_none() {
                let target_start = NodeId::from_u32(rng.gen_range(0..self.rail.nodes.len()) as u32);
                let links: Vec<_> = self.rail.links.get_links(target_start).collect();
                let &(target_end, _) = links.choose(&mut rng).unwrap();
                train.target = Some(TrackLoc::new(target_start, target_end, 0.));
            }
        }
    }
}
