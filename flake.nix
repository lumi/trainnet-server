{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };
  
  outputs = { nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs { inherit system; };
    in {
      devShells.default = pkgs.mkShell {
        nativeBuildInputs = with pkgs; [
          # Rust stuff
          rustc
          cargo
          rust-analyzer
          rustfmt

          # Common dependencies
          pkg-config
          openssl

          # Web
          caddy

          # Debug
          websocat
        ];
      };
    }) // {
      pognix.extraModules = [({ ... }: {
        pognix.network.mappings = [
          "5454:5454"
        ];
      })];
    };
}
